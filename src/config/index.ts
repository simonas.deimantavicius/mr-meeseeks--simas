import { config as dotEnvConfig } from 'dotenv';

import { castToBool } from '../utils';

dotEnvConfig();

const config = {
  toTheLetter: castToBool(process.env.TO_THE_LETTER),
};

export { config };
