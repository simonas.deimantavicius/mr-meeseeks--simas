import { RealyAny } from 'types';

const castToBool = (value: RealyAny): boolean => {
  switch (typeof value) {
    case 'string':
      return value.toLowerCase() === 'true';

    case 'number':
      return value !== 0 ? true : false;

    case 'boolean':
      return value;

    default:
      return !!value;
  }
};

export { castToBool };
